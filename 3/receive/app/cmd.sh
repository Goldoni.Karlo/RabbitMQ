#!/bin/bash
#Environment variable RABBITMQWFI must have a string like "rabbitmqserver:5672 -t 10 -- npm start"
#exec echo $RABBITMQWFI
exec ./wait-for-it.sh $RABBITMQWFI
#set -e
#if [ "$ENV" = 'DEV' ]; then
#    echo "Running Development Server" # Запуск сервера для разработки
#    exec python "identidock.py"
#else
#    echo "Running Production Server" # Запуск сервера для эксплуатации
#    exec uwsgi --http 0.0.0.0:9090 --wsgi-file /app/identidock.py \
#        --callable app --stats 0.0.0.0:9191
#fi
#exec npm start
