//var myhttp=require('./testserver');
// Receiver work with Exchange fanout
var amqp = require('amqplib/callback_api');
var RABBITMQURL=process.env.RABBITMQURL;
amqp.connect(RABBITMQURL, function(err, conn) {
  console.log(err);
  conn.createChannel(function(err, ch) {
      var ex = 'logs';
      ch.assertExchange(ex, 'fanout', {durable: false});
      ch.assertQueue('', {exclusive: true}, function(err, q) {
        console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", q.queue);
        ch.bindQueue(q.queue, ex, '');
  
        ch.consume(q.queue, function(msg) {
          console.log(" [x] %s", msg.content.toString());
        }, {noAck: true});
      });
    });
  });
