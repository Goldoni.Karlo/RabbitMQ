//var myhttp=require('./testserver');

var amqp = require('amqplib/callback_api');
var RABBITMQURL=process.env.RABBITMQURL;
amqp.connect(RABBITMQURL, function(err, conn) {
  console.log(err);
  conn.createChannel(function(err, ch) {
    var ex = 'logs';
    ch.assertExchange(ex, 'fanout', {durable: false});
    var msg=['First message.','Second message..', 'Third message...', 'Fourth message....', 'Fifth message.....'];
    for (var i=0; i<msg.length; i++)
      {
        ch.publish(ex, '', new Buffer(msg[i]));
        console.log(" [x] Sent %s", msg[i]);
      }
  });
  setTimeout(function() { conn.close(); process.exit(0) }, 500);
});
