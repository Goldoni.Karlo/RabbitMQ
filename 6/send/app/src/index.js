//var myhttp=require('./testserver');

var amqp = require('amqplib/callback_api');
var RABBITMQURL=process.env.RABBITMQURL;
amqp.connect(RABBITMQURL, function(err, conn) {
  console.log(err);
  conn.createChannel(function(err, ch) {
    ch.assertQueue('', {exclusive: true}, function(err, q) {
      var corr = generateUuid();
      var nums = [15, 3, 34, 42];
  
      ch.consume(q.queue, function(msg) {
        if (msg.properties.correlationId === corr) {
          console.log(' [.] Got %s', msg.content.toString());
         // setTimeout(function() { conn.close(); process.exit(0) }, 500);
        }
      }, {noAck: true});
      for (var i=0; i<nums.length; i++)
      {
        console.log(' [x] Requesting fib(%d)', nums[i]);
        ch.sendToQueue('rpc_queue', new Buffer(nums[i].toString()), { correlationId: corr, replyTo: q.queue });
      }  
    });
  });
});

function generateUuid() {
  return Math.random().toString() +
         Math.random().toString() +
         Math.random().toString();
}