//var myhttp=require('./testserver');

var amqp = require('amqplib/callback_api');
var RABBITMQURL=process.env.RABBITMQURL;
amqp.connect(RABBITMQURL, function(err, conn) {
  console.log(err);
  conn.createChannel(function(err, ch) {
    var ex = 'direct_logs';
      
    ch.assertExchange(ex, 'direct', {durable: false});
               
    var msg=['First message', 'Second message', 'Third message','Fourth message....','Fifth message'];
    var type=['info', 'warning', 'error', 'info', 'error'];
    
    for (var i=0; i<msg.length; i++)
      {
         ch.publish(ex, type[i], new Buffer(msg[i]));
         console.log(" [x] Sent %s", msg[i], type[i]);   
      }
  });
  setTimeout(function() { conn.close(); process.exit(0) }, 500);
});