//var myhttp=require('./testserver');

var keys=process.env.KEYS.split(',');
console.log(keys);
var amqp = require('amqplib/callback_api');
var RABBITMQURL=process.env.RABBITMQURL;

amqp.connect(RABBITMQURL, function(err, conn) {
  console.log(err);
  conn.createChannel(function(err, ch) {
    var ex = 'direct_logs';
    ch.assertExchange(ex, 'direct', {durable: false});
  
    ch.assertQueue('', {exclusive: true}, function(err, q) {
        console.log(' [*] Waiting for logs. To exit press CTRL+C');
  
    keys.forEach(function(severity) {
          ch.bindQueue(q.queue, ex, severity);
        });
  
        ch.consume(q.queue, function(msg) {
          console.log(" [x] %s: '%s'", msg.fields.routingKey, msg.content.toString());
        }, {noAck: true});
      });
    });
  });