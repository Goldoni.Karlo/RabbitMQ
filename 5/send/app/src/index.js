//var myhttp=require('./testserver');

var amqp = require('amqplib/callback_api');
var RABBITMQURL=process.env.RABBITMQURL;
amqp.connect(RABBITMQURL, function(err, conn) {
  console.log(err);
  conn.createChannel(function(err, ch) {
    var ex = 'topics_logs';
      
    ch.assertExchange(ex, 'topic', {durable: false});
                 
    var msg=['Anonimous info mess', 'Kernel warning mess', 'Cron error mess','Kernel info mess','Anonimous error mess'];
    var key=['anonimous.info', 'kernel.warning', 'cron.error', 'kernel.info', 'anonimous.error'];
    
    for (var i=0; i<msg.length; i++)
      {
         ch.publish(ex, key[i], new Buffer(msg[i]));
         console.log(" [x] Sent %s", msg[i], key[i]);   
      }
  });
  setTimeout(function() { conn.close(); process.exit(0) }, 500);
});